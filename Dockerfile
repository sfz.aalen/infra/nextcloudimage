FROM ubuntu:latest AS downloader

RUN apt update
RUN apt-get install -y curl

RUN mkdir -p /opt/bitnami/mediawiki/extensions

# PluggableAuth
RUN cd /tmp/; curl -LJO "https://github.com/wikimedia/mediawiki-extensions-PluggableAuth/archive/master.tar.gz" && cd /tmp/; mv *.tar.gz /tmp/plugin.tar.gz && \
    tar -xzf /tmp/plugin.tar.gz -C /opt/bitnami/mediawiki/extensions && \
    rm /tmp/plugin.tar.gz

# OpenIDConnect
RUN cd /tmp/; curl -LJO "https://github.com/wikimedia/mediawiki-extensions-OpenIDConnect/archive/master.tar.gz" && cd /tmp/; mv *.tar.gz /tmp/plugin.tar.gz  && \
    tar -xzf /tmp/plugin.tar.gz -C /opt/bitnami/mediawiki/extensions && \
    rm /tmp/plugin.tar.gz

# CommonsMetadata
RUN cd /tmp/; curl -LJO "https://github.com/wikimedia/mediawiki-extensions-CommonsMetadata/archive/master.tar.gz" && cd /tmp/; mv *.tar.gz /tmp/plugin.tar.gz  && \
    tar -xzf /tmp/plugin.tar.gz -C /opt/bitnami/mediawiki/extensions && \
    rm /tmp/plugin.tar.gz

# MultimediaViewer
RUN cd /tmp/; curl -LJO "https://github.com/wikimedia/mediawiki-extensions-MultimediaViewer/archive/master.tar.gz" && cd /tmp/; mv *.tar.gz /tmp/plugin.tar.gz  && \
    tar -xzf /tmp/plugin.tar.gz -C /opt/bitnami/mediawiki/extensions && \
    rm /tmp/plugin.tar.gz


FROM docker.io/bitnami/mediawiki:latest
USER root
RUN install_packages imagemagick
USER 1001

COPY --from=downloader /opt/bitnami/mediawiki/extensions /opt/bitnami/mediawiki/extensions
# Install dependencies
#ADD composer.local.json /opt/bitnami/mediawiki/composer.local.json
#RUN cd /opt/bitnami/mediawiki && composer clearcache && composer config --no-plugins allow-plugins.composer/installers true && 

#ADD composer.local.json /opt/bitnami/mediawiki/composer.local.json

RUN cd /opt/bitnami/mediawiki && composer config --no-plugins allow-plugins.composer/installers true && composer require --no-update mediawiki/maps:~10.1 && composer update

#RUN cd /opt/bitnami/mediawiki && find -name "*composer.lock" -delete
